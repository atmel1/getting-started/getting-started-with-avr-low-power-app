/*
 * Getting_Started_with_AVR_Low_Power_App.c
 *
 * Created: 3/29/2015 23:48:00
 *  Author: Brandy
 */ 

#define F_CPU 1000000UL
#include <avr/io.h>
#include <avr/sleep.h>
#include <util/delay.h>
#include <avr/interrupt.h>
ISR(PCINT0_vect)
{
	PINB |= (1<<DDB2);
}

int main(void)
{
	DDRB |= (1<<DDB2)|(1<<DDB0);
	DDRB &= ~(1<<DDB1);
	PRR |=0xFF;
	PCMSK |= (1<<PCINT0);
	GIMSK |=(1<<PCIE);
	/*
		� � � � PRTIM1 PRTIM0 PRUSI PRADC
		Bits 7:4 Reserved Bits
		Power Reduction Timer/Counter1
			Writing a logic one to this bit shuts down the Timer/Counter1 module. 
			When the Timer/Counter1 is enabled, operation will continue like before the shutdown.
		Power Reduction Timer/Counter0
		Power Reduction USI
			Writing a logic one to this bit shuts down the USI by stopping the clock to the module. 
			When waking up the USI again, the USI should be re initialized to ensure proper operation.
		Power Reduction ADC
			Writing a logic one to this bit shuts down the ADC. The ADC must be disabled before shut down. 
			Note that the ADC clock is also used by some parts of the analog comparator, 
			which means that the analogue comparator can not be used when this bit is high.
		pg.38
	*/
	set_sleep_mode(SLEEP_MODE_PWR_DOWN);
	sei();
    while(1)
    {
		PINB |=(1<<DDB0);
		sleep_mode();
        //TODO:: Please write your application code 
    }
}